import {getLocal } from '@/utils/local'
import store from '../store'
import * as types from '@/store/action-types'
class WS {
    constructor(config = {}) {
        this.url = config.url || 'vue.zhufengpeixun.cn'; 
        this.port = config.prot || 80;
        this.protocol = config.protocol || 'ws';
        this.time = config.time || 30 * 1000;
        this.ws = null;
    }
    onOpen = () => {
        console.log("onOpen");
        let token =  getLocal('token');
        console.log(token);
        this.ws.send(JSON.stringify({
            type:'auth',
            data: token
        }))
    }
    onMessage = (e) => {
        console.log("onMessage");
        let {type,data} = JSON.parse(e.data);
        console.log(type);
        console.log(data);
        switch(type){
            case 'noAuth':
                console.log('没有权限');
                break;
            case 'heartCheck':
                this.checkServer();
                this.ws.send(JSON.stringify({type:'heartCheck'}));
                break;
            default:
                store.commit(types.SET_MESSAGE,data);
        }

    }
    onError = () => {
        console.log("onError");
        setTimeout(() => {
            this.create();
        }, 1000);
    }
    onClose = () => {
        console.log("onClose");
        this.ws.close();
    }
    create() {
        console.log("create");
        this.ws = new WebSocket(`${this.protocol}://${this.url}:${this.port}`);
        this.ws.onopen = this.onOpen;
        this.ws.onmessage = this.onMessage;
        this.ws.onclose = this.onclose;
        this.ws.onerror = this.onError
    }
    checkServer(){
        console.log("checkServer");
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
            this.onClose();
            this.onError();
        }, this.time + 1000);
    }
    send = (msg)=>{
        console.log("send");
        this.ws.send(JSON.stringify(msg))
    }
}
export default WS;
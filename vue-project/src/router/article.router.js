export default [{
    path: '/post',
    name: 'post',
    component: () => import('@/views/manager/infoPublish.vue'),
    meta:{
        needLogin:true
    }
}]
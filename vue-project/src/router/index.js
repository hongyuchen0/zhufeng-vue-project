import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import hooks from './hooks';

Vue.use(VueRouter)

/* const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/404',
    name: '404',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('../views/404.vue')
  }
] */

//require.context()是webpack语法，创建自己的（模块）上下文。
//三个参数：搜索文件夹目录，是否搜索子目录，匹配文件的正则表达式
const files = require.context('./',false,/\.router.js$/);
// console.log('files', files);
const routes = [];
files.keys().forEach(key=>{
  routes.push(...files(key).default);
});
// console.log('routes', routes);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
Object.values(hooks).forEach(hook=>{
  router.beforeEach(hook.bind(router));
});

export default router
